package com.globallogic.restapiUser.constants;


public class LoggerMessages {

  /**
   * Instantiates a new logger messages.
   */
  private LoggerMessages() {
    throw new IllegalStateException(ExceptionMessages.MSG_EXCEPTION_CONSTANTS);
  }

  /** The Constant TRACE_METHOD_ENTERING. */
  /* TRACE */
  public static final String TRACE_METHOD_ENTERING = "Entering the method {}";

  /** The Constant TRACE_METHOD_LEAVING. */
  public static final String TRACE_METHOD_LEAVING = "Leaving the method {}";

 

  /** The Constant ERROR_HTML_NOT_GENERATED. */
  /* ERROR */
  public static final String ERROR_CREATE_USER = "The user exists";


}
