package com.globallogic.restapiUser.exception;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ExcecutionExceptionMessage {

  //manejamos y orientamos la salida
  @JsonProperty("message")
  private String mensaje;
  @JsonProperty("codigo")
  private int codigo;
   public ExcecutionExceptionMessage(String mensaje, int codigo) {

    this.mensaje = mensaje;
    this.codigo = codigo;
  }
  
  
}
