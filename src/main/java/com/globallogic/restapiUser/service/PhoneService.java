package com.globallogic.restapiUser.service;

import java.util.Collection;
import java.util.List;
import com.globallogic.restapiUser.dto.PhoneDTO;
import com.globallogic.restapiUser.model.Phones;
import com.globallogic.restapiUser.model.Users;

public interface PhoneService {

  public Collection<Phones> save(PhoneDTO phone, Users users) throws Exception;
  public Collection<Phones> save(List<Phones> phones , Users users) throws Exception;
}
