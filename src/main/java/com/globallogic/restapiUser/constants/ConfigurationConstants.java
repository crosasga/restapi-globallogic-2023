package com.globallogic.restapiUser.constants;

/**
 * Class ConfigurationConstants
 * 
 * @author barcasfe
 *
 */
public class ConfigurationConstants {

  private ConfigurationConstants() {
    throw new IllegalStateException(ExceptionMessages.MSG_EXCEPTION_CONSTANTS);
  }


  public static final String DATE_FORMAT = "yyyyMMddHHmmss";
  public static final String PASSWORD_FORMAT_JSON= "^(?=(?:.*\\d){2})(?=(?:.*[A-Z]){1})(?=(?:.*[a-z]){1})\\S{4,}$";
  
  public static final String PASSWORD_FORMAT_JSON2= "^(?=(?:.*\\d){2})(?=(?:.*[A-Z]){2})(?=(?:.*[a-z]){2})\\S{8,}$";


  public static final String SLASH = "/";
}
