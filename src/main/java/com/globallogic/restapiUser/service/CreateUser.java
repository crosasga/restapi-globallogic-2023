package com.globallogic.restapiUser.service;

import java.util.Collection;
import com.globallogic.restapiUser.dto.PhoneDTO;
import com.globallogic.restapiUser.dto.UserDTO;
import com.globallogic.restapiUser.model.Users;

public interface CreateUser {
  public Users create(UserDTO userDTO, Collection<PhoneDTO> phoneDTOs) throws  Exception;
  public Users create(UserDTO userDTO) throws  Exception ;
}
