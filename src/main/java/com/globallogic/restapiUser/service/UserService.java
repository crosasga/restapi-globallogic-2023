package com.globallogic.restapiUser.service;

import com.globallogic.restapiUser.dto.UserDTO;
import com.globallogic.restapiUser.model.Users;

public interface UserService {

  public Users save(UserDTO userDTO) throws Exception ;
  public void usuarioExiste(String email) throws  Exception;
}
