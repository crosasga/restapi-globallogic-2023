package com.globallogic.restapiUser.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.globallogic.restapiUser.model.Phones;

@Repository
public interface PhoneRepository extends JpaRepository<Phones, String> {

}
