package com.globallogic.restapiUser.generator;

import java.util.ArrayList;
import java.util.Collection;
import com.globallogic.restapiUser.dto.PhoneDTO;
import com.globallogic.restapiUser.dto.UserDTO;


public class DataDumy {

  public UserDTO getRegisterAllData() {
    UserDTO userDTO = new UserDTO();
    userDTO.setName("Alan brito");
    userDTO.setEmail("alan.brito_cl@gmail.com");
    userDTO.setPassword("Herna12");
    userDTO.setPhoneDTOs(getPhone());
    return userDTO;
}
  public UserDTO getRegisterAllWrongPassword() {
    UserDTO userDTO = new UserDTO();
    userDTO.setName("Alan brito");
    userDTO.setEmail("alan.brito@gmail.com");
    userDTO.setPassword("alanbrito");
    userDTO.setPhoneDTOs(getPhone());
    return userDTO;
}  
  public Collection<PhoneDTO> getPhone() {
    Collection<PhoneDTO> phoneDtos = new ArrayList<>();
    PhoneDTO phoneDto = new PhoneDTO();
    phoneDto.setNumber("1234567890");
    phoneDto.setCityCode("1");
    phoneDto.setCountryCode("56");
    phoneDtos.add(phoneDto);
    return phoneDtos;
}
  
}
