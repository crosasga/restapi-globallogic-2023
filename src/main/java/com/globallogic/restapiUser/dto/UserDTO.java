package com.globallogic.restapiUser.dto;

import java.util.Collection;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.globallogic.restapiUser.constants.ConfigurationConstants;
import com.globallogic.restapiUser.constants.Message;
import lombok.Data;

@Data
public class UserDTO {
  
  @NotNull(message = "Nombre " +Message.MESSAGE_NO_NULL)
  private String name;
  
  @NotNull(message = "Email " + Message.MESSAGE_NO_NULL)
  @Email
  private String email;
  
  @NotNull(message ="Password " + Message.MESSAGE_NO_NULL)
  @Pattern(regexp = ConfigurationConstants.PASSWORD_FORMAT_JSON, message = Message.MESSAGE_EMAIL_FORMAT)
  private String password;
  
  private String token;
  
  @Valid
  @NotNull(message = Message.MESSAGE_NO_NULL)
  @Size(min = 1, message = Message.MESSAGE_PHONES_SIZE)
  @JsonProperty("phones")
  private Collection<PhoneDTO> phoneDTOs;

}
