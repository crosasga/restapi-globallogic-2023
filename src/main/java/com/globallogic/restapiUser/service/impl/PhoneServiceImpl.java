package com.globallogic.restapiUser.service.impl;

import java.util.Collection;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.globallogic.restapiUser.constants.LoggerMessages;
import com.globallogic.restapiUser.dto.PhoneDTO;
import com.globallogic.restapiUser.model.Phones;
import com.globallogic.restapiUser.model.Users;
import com.globallogic.restapiUser.repository.PhoneRepository;
import com.globallogic.restapiUser.service.PhoneService;
import com.globallogic.restapiUser.util.DtoPojo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@AllArgsConstructor
@Transactional(readOnly = false)
public class PhoneServiceImpl implements PhoneService {
  
  @Autowired
  private PhoneRepository phoneRepository;
  
  @Transactional
  public Collection<Phones> save(PhoneDTO phone, Users users) throws Exception{
    log.trace(LoggerMessages.TRACE_METHOD_ENTERING, "save phones");
    log.info("Users ",  users.toString());
    log.info("PhoneDTO ",  phone.toString());
    //Phones phones = DtoPojo.convert(userDTO);
    Collection<Phones> phones= DtoPojo.addPhoneToUSer(phone, users);
    log.trace(LoggerMessages.TRACE_METHOD_LEAVING, "save phones");
    phoneRepository.saveAll(phones);
    return phones;
  }

  @Override
  public Collection<Phones> save(List<Phones> phones, Users users) throws Exception {

    return null;
  }
  


}
