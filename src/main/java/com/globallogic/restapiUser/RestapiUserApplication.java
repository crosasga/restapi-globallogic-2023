package com.globallogic.restapiUser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import com.globallogic.restapiUser.config.security.JWTAuthorizationFilter;
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
/*
 * @Configuration
 * 
 * @EnableJpaRepositories
 * 
 * @EnableWebSecurity
 */
public class RestapiUserApplication {

  public static void main(String[] args) {
    SpringApplication.run(RestapiUserApplication.class, args);
  }

  @EnableWebSecurity
  @Configuration
  class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
      http.csrf().disable()
          .addFilterAfter(new JWTAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class)
          .authorizeRequests().antMatchers(HttpMethod.POST, "/register").permitAll().anyRequest()
          .authenticated();
    }
  }

}
