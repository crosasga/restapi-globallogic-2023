package com.globallogic.restapiUser.exception.advice;

import javax.servlet.http.HttpServletRequest;

import javassist.tools.web.BadHttpRequest;
import org.junit.platform.commons.PreconditionViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import com.globallogic.restapiUser.exception.ExcecutionExceptionMessage;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.rmi.ServerException;

@ControllerAdvice
public class ErrorHandler {

   @ExceptionHandler(MethodArgumentNotValidException.class)
   public ResponseEntity<ExcecutionExceptionMessage> methodArgumentNotValidException(HttpServletRequest request, MethodArgumentNotValidException e) {
       ExcecutionExceptionMessage errorMessage = new ExcecutionExceptionMessage(e.getMessage(),HttpStatus.BAD_REQUEST.value());
       return new ResponseEntity<>(errorMessage, new HttpHeaders(), HttpStatus.BAD_REQUEST);
   }
    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<ExcecutionExceptionMessage> accessDeniedException(HttpServletRequest request, Exception e) {
        ExcecutionExceptionMessage errorMessage = new ExcecutionExceptionMessage(e.getMessage(),HttpStatus.FORBIDDEN.value());
        return new ResponseEntity<>(errorMessage, new HttpHeaders(), HttpStatus.FORBIDDEN);
    }
    @ExceptionHandler(ServerException.class)
    public ResponseEntity<ExcecutionExceptionMessage> serverException(HttpServletRequest request, Exception e) {
        ExcecutionExceptionMessage errorMessage = new ExcecutionExceptionMessage(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR.value());
        return new ResponseEntity<>(errorMessage, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
    @ExceptionHandler(BadHttpRequest.class)
    public ResponseEntity<ExcecutionExceptionMessage> badRequestException(HttpServletRequest request, Exception e) {
        ExcecutionExceptionMessage errorMessage = new ExcecutionExceptionMessage(e.getMessage(),HttpStatus.BAD_REQUEST.value());
        return new ResponseEntity<>(errorMessage, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<ExcecutionExceptionMessage> handleMethodArgumentTypeMismatch(HttpServletRequest request, Exception e) {
        ExcecutionExceptionMessage errorMessage = new ExcecutionExceptionMessage(e.getMessage(),HttpStatus.METHOD_NOT_ALLOWED.value());
        return new ResponseEntity<>(errorMessage, new HttpHeaders(), HttpStatus.METHOD_NOT_ALLOWED);
    }
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ResponseEntity<ExcecutionExceptionMessage> requestNotAllow(HttpServletRequest request, Exception e) {
        ExcecutionExceptionMessage errorMessage = new ExcecutionExceptionMessage(e.getMessage(),HttpStatus.NOT_FOUND.value());
        return new ResponseEntity<>(errorMessage, new HttpHeaders(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(PreconditionViolationException.class)
    public ResponseEntity<ExcecutionExceptionMessage> preconditionViolationException(HttpServletRequest request, Exception e) {
        ExcecutionExceptionMessage errorMessage = new ExcecutionExceptionMessage(e.getMessage(),HttpStatus.PRECONDITION_REQUIRED.value());
        return new ResponseEntity<>(errorMessage, new HttpHeaders(), HttpStatus.PRECONDITION_REQUIRED);
    }

    @ExceptionHandler(Exception.class)
   public ResponseEntity<ExcecutionExceptionMessage> eException(HttpServletRequest request, Exception e) {
     ExcecutionExceptionMessage errorMessage = new ExcecutionExceptionMessage(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR.value());
     return new ResponseEntity<>(errorMessage, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
   }
}